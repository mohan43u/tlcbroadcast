package tlcbroadcast

import (
	"log"
	"time"

	"google.golang.org/api/youtube/v3"
)

type YtVideo struct {
	Id string
	Url string
	PubTime time.Time
	Title string
	Description string
	Status string
}

func ytGetVideoIds(yclient *youtube.Service) []string {
	ids := make([]string, 0)
	vservice := youtube.NewSearchService(yclient)
	call := vservice.List([]string{"snippet"})
	call = call.ForMine(true)
	call = call.MaxResults(10000)
	call = call.Type("video")
	call = call.Order("date")
	call = call.Fields("items(id(videoId))")
	for {
		response, err := call.Do()
		if err != nil {
			log.Printf("failed to get videos: %+v %+v\n", call, err)
			return nil
		}
		for _, item := range response.Items {
			ids = append(append(make([]string, 0), item.Id.VideoId), ids...)
		}
		if len(response.NextPageToken) > 0 {
			call = call.PageToken(response.NextPageToken)
		} else {
			break
		}
	}
	return ids
}

func YtGetVideos(ytclient *youtube.Service) *[]YtVideo {
	ids := ytGetVideoIds(ytclient)
	if ids == nil {
		log.Println("not able to get video ids")
		return nil
	}
	videos := make([]YtVideo, 0)
	vservice := youtube.NewVideosService(ytclient)
	call := vservice.List([]string{"snippet", "status"})
	call = call.Id(ids...)
	call = call.MaxResults(10000)
	call = call.Fields("items(id,snippet(publishedAt,title,description),status(privacyStatus))")
	for {
		response, err := call.Do()
		if err != nil {
			log.Printf("failed to get videos: %+v %+v\n", call, err)
			return nil
		}
		for _, item := range response.Items {
			video := &YtVideo{}
			video.Id = item.Id
			video.Url = "https://www.youtube.com/watch?v=" + item.Id
			video.PubTime, err = time.Parse(time.RFC3339, item.Snippet.PublishedAt)
			if err != nil {
				log.Printf("failed to parse PublishedAt: %+v\n", err)
			}
			video.Title = item.Snippet.Title
			video.Description = item.Snippet.Description
			video.Status = item.Status.PrivacyStatus
			videos = append(videos, *video)
		}
		if len(response.NextPageToken) > 0 {
			call = call.PageToken(response.NextPageToken)
		} else {
			break
		}
	}
	return &videos
}
