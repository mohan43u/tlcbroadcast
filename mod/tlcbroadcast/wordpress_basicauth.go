package tlcbroadcast

import (
	"io"
	"os"
	"log"
	"os/user"
	"net/http"
	"path/filepath"
	"encoding/json"
)

type WpBasicAuth struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type WpBasicAuthClient struct {
	client *http.Client
	basicauth *WpBasicAuth
}

func wpGetWpBasicAuthClient(basicauth *WpBasicAuth) *WpBasicAuthClient {
	basicauthclient := &WpBasicAuthClient{}
	basicauthclient.basicauth = basicauth
	basicauthclient.client = http.DefaultClient
	return basicauthclient
}

func (self *WpBasicAuthClient) Get(url string) (resp *http.Response, err error) {
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	request.SetBasicAuth(self.basicauth.Username, self.basicauth.Password)
	return self.client.Do(request)
}

func (self *WpBasicAuthClient) Post(url string, contentType string, body io.Reader) (resp *http.Response, err error) {
	request, err := http.NewRequest(http.MethodPost, url, body)
	if err != nil {
		return nil, err
	}
	request.SetBasicAuth(self.basicauth.Username, self.basicauth.Password)
	request.Header.Add("Content-Type", contentType)
	return self.client.Do(request)
}

func wpBasicAuthConfigFile() (string, error) {
	usr, err := user.Current()
	if err != nil {
		return "", err
	}
	configDir := os.Getenv("XDG_CONFIG_HOME")
	if len(configDir) <= 0 {
		configDir = filepath.Join(usr.HomeDir, ".config")
	}
	configDir = filepath.Join(configDir, "tlcbroadcast")
	os.MkdirAll(configDir, 0700)
	return filepath.Join(configDir, "wpbasicauthclient.json"), err
}

func wpBasicAuthFromFile(file string) (*WpBasicAuth, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	basicauth := &WpBasicAuth{}
	err = json.NewDecoder(f).Decode(basicauth)
	return basicauth, err
}

func WpGetClientBasicAuth() *WpClient {
	basicauthfile, err := wpBasicAuthConfigFile()
	if err != nil {
		log.Printf("Unable to get basic auth filename: %v", err)
		return nil
	}
	basicauth, err := wpBasicAuthFromFile(basicauthfile)
	if err != nil {
		log.Printf("Unable to get basic auth: %v", err)
		return nil
	}
	wpbasicauthclient := wpGetWpBasicAuthClient(basicauth)
	wpclient := &WpClient{}
	wpclient.Client = wpbasicauthclient
	wpclient.Urls.MeUrlPathTemplate = "https://%v/wp-json/wp/v2/users/me"
	wpclient.Urls.PostsUrlPathTemplate = "https://%v/wp-json/wp/v2/posts"
	wpclient.Urls.NewPostUrlPathTemplate = "https://%v/wp-json/wp/v2/posts"
	wpclient.Urls.TagUrlPathTemplate = "https://%v/wp-json/wp/v2/tags?search=%v"
	wpclient.Urls.NewTagUrlPathTemplate = "https://%v/wp-json/wp/v2/tags"
	wpclient.Urls.CategoryUrlPathTemplate = "https://%v/wp-json/wp/v2/categories?search=%v"
	wpclient.Urls.NewCategoryUrlPathTemplate = "https://%v/wp-json/wp/v2/categories"
	return wpclient
}
