package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/mohan43u/tlcbroadcast/mod/tlcbroadcast"
)

func checkAndPost(now time.Time, wordpress_website string, discourse_website string) bool {
	fmt.Println(now)

	ytclient := tlcbroadcast.YtGetClient()
	if ytclient == nil {
		fmt.Println("not able to initialize ytclient")
		return false
	}
	videos := tlcbroadcast.YtGetVideos(ytclient)
	if videos == nil {
		fmt.Println("error getting videos")
		return false
	}

	wpclient := tlcbroadcast.WpGetClient()
	if wpclient == nil {
		fmt.Println("not able to initialize wpclient")
	} else {
		wpclient.Upload(wordpress_website, videos)
	}

	dcclient := tlcbroadcast.DcGetClient(discourse_website)
	if dcclient == nil {
		fmt.Println("not able to initialize dcclient")
	} else {
		dcclient.Upload(videos)
	}

	return true
}

func main() {
	if len(os.Args) < 3 {
		fmt.Println("[usage]", os.Args[0], "wordpress-site", "discourse-website")
		return
	}
	if !checkAndPost(time.Now(), os.Args[1], os.Args[2]) {
		fmt.Println("failed to upload")
	}
}
