package tlcbroadcast

import (
	"os"
	"log"
	"os/user"
	"encoding/json"
	"path/filepath"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"
)

type WpConfig struct {
	ClientId string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	RedirectUrls []string `json:"redirect_urls"`
	RequestTokenUrl string `json:"request_token_url"`
	AuthorizeUrl string `json:"authorize_url"`
	AuthenticateUrl string `json:"authenticate_url"`
}

func wpconfigFile() (string, error) {
	usr, err := user.Current()
	if err != nil {
		return "", err
	}
	configDir := os.Getenv("XDG_CONFIG_HOME")
	if len(configDir) <= 0 {
		configDir = filepath.Join(usr.HomeDir, ".config")
	}
	configDir = filepath.Join(configDir, "tlcbroadcast")
	os.MkdirAll(configDir, 0700)
	return filepath.Join(configDir,"wpclient.json"), err
}

func wptokenCacheFile() (string, error) {
	usr, err := user.Current()
	if err != nil {
		return "", err
	}
	tokenCacheDir := os.Getenv("XDG_CACHE_HOME")
	if len(tokenCacheDir) <= 0 {
		tokenCacheDir = filepath.Join(usr.HomeDir, ".cache")
	}
	tokenCacheDir = filepath.Join(tokenCacheDir, "tlcbroadcast")
	os.MkdirAll(tokenCacheDir, 0700)
	return filepath.Join(tokenCacheDir,"wptoken.json"), err
}

func wptokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	t := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(t)
	return t, err
}

func wpgetTokenFromWeb(ctx context.Context, oauth2config *oauth2.Config) *oauth2.Token {
	authurl := oauth2config.AuthCodeURL("state-token", oauth2.SetAuthURLParam("response_type", "code"))
	code := GetAuthCode(authurl)
	if len(code) <= 0 {
		log.Println("Failed to get auth code")
		return nil
	}
	tok, err := oauth2config.Exchange(ctx, code)
	if err != nil {
		log.Printf("Unable to retrieve token from web %v", err)
	}
	return tok
}

func wpsaveToken(file string, token *oauth2.Token) {
	log.Printf("Saving credentials to: %s\n", file)
	f, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Printf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func WpGetClientOauth() *WpClient {
	configfile, err := wpconfigFile()
	if err != nil {
		log.Printf("not able to get config file name: %v", err)
		return nil
	}
	config, err := os.ReadFile(configfile)
	if err != nil {
		log.Printf("Unable to read config file: %v", err)
		return nil
	}

	wpconfig := &WpConfig{
		ClientId:        "",
		ClientSecret:    "",
		RedirectUrls:    []string{},
		RequestTokenUrl: "",
		AuthorizeUrl:    "",
		AuthenticateUrl: "",
	}
	err = json.Unmarshal([]byte(config), wpconfig)
	if err != nil {
		log.Printf("Unable to parse config file: %v", err)
		return nil
	}
	oauth2config := &oauth2.Config{
		ClientID: wpconfig.ClientId,
		ClientSecret: wpconfig.ClientSecret,
		Endpoint: oauth2.Endpoint{
			AuthURL: wpconfig.AuthorizeUrl,
			TokenURL: wpconfig.RequestTokenUrl,
		},
		RedirectURL: wpconfig.RedirectUrls[0],
		Scopes: []string{},
	}
	cacheFile, err := wptokenCacheFile()
	if err != nil {
		log.Printf("Unable to get path to cached credential file. %v", err)
		return nil
	}
	ctx := context.Background()
	tok, err := wptokenFromFile(cacheFile)
	if err != nil {
		tok = wpgetTokenFromWeb(ctx, oauth2config)
		if tok == nil {
			log.Printf("unable to get auth token\n")
			return nil
		}
		wpsaveToken(cacheFile, tok)
	}
	log.Println("[wptoken] Expirey: ", tok.Expiry, ", isZero: ", tok.Expiry.IsZero(), ", isValid: ", tok.Valid())
	tokSource := oauth2config.TokenSource(ctx, tok)
	ntok, err := tokSource.Token()
	if err != nil {
		log.Printf("unable to get access token %v", err)
		return nil
	}
	if ntok.AccessToken != tok.AccessToken {
		wpsaveToken(cacheFile, ntok)
	}

	wpclient := &WpClient{}
	wpclient.Client = oauth2config.Client(ctx, ntok)
	wpclient.Urls.MeUrlPathTemplate = "https://public-api.wordpress.com/rest/v1.1/me"
	wpclient.Urls.PostsUrlPathTemplate = "https://public-api.wordpress.com/rest/v1.1/sites/%v/posts"
	wpclient.Urls.NewPostUrlPathTemplate = "https://public-api.wordpress.com/rest/v1.1/sites/%v/posts/new"
	wpclient.Urls.TagUrlPathTemplate = "https://public-api.wordpress.com/rest/v1.1/sites/%v/tags/slug:%v"
	wpclient.Urls.NewTagUrlPathTemplate = "https://public-api.wordpress.com/rest/v1.1/sites/%v/tags/new"
	wpclient.Urls.CategoryUrlPathTemplate = "https://public-api.wordpress.com/rest/v1.1/sites/%v/categories/slug:%v"
	wpclient.Urls.NewCategoryUrlPathTemplate = "https://public-api.wordpress.com/rest/v1.1/sites/%v/categories/new"
	return wpclient
}
