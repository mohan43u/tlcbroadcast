package tlcbroadcast

import (
	"os"
	"log"
	"os/user"
	"path/filepath"
	"encoding/json"

	"golang.org/x/oauth2"
	"golang.org/x/net/context"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/youtube/v3"
)

func ytconfigFile() (string, error) {
	usr, err := user.Current()
	if err != nil {
		return "", err
	}
	configDir := os.Getenv("XDG_CONFIG_HOME")
	if len(configDir) <= 0 {
		configDir = filepath.Join(usr.HomeDir, ".config")
	}
	configDir = filepath.Join(configDir, "tlcbroadcast")
	os.MkdirAll(configDir, 0700)
	return filepath.Join(configDir,"ytclient.json"), err
}

func yttokenCacheFile() (string, error) {
	usr, err := user.Current()
	if err != nil {
		return "", err
	}
	tokenCacheDir := os.Getenv("XDG_CACHE_HOME")
	if len(tokenCacheDir) <= 0 {
		tokenCacheDir = filepath.Join(usr.HomeDir, ".cache")
	}
	tokenCacheDir = filepath.Join(tokenCacheDir, "tlcbroadcast")
	os.MkdirAll(tokenCacheDir, 0700)
	return filepath.Join(tokenCacheDir,"yttoken.json"), err
}

func yttokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	t := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(t)
	return t, err
}

func ytgetTokenFromWeb(ctx context.Context, oauth2config *oauth2.Config) *oauth2.Token {
	authurl := oauth2config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	code := GetAuthCode(authurl)
	if len(code) <= 0 {
		log.Println("Failed to get auth code")
		return nil
	}
	tok, err := oauth2config.Exchange(ctx, code)
	if err != nil {
		log.Printf("Unable to retrieve token from web %v", err)
	}
	return tok
}

func ytsaveToken(file string, token *oauth2.Token) {
	log.Printf("Saving credentials to: %s\n", file)
	f, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Printf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func YtGetClient() *youtube.Service {
	configfile, err := ytconfigFile()
	if err != nil {
		log.Printf("not able to get config file name: %v", err)
		return nil
	}
	config, err := os.ReadFile(configfile)
	if err != nil {
		log.Printf("Unable to read config file: %v", err)
		return nil
	}
	oauth2config, err := google.ConfigFromJSON(config, youtube.YoutubeReadonlyScope)
	if err != nil {
		log.Printf("Unable to parse config file: %v", err)
		return nil
	}
	cacheFile, err := yttokenCacheFile()
	if err != nil {
		log.Printf("Unable to get path to cached credential file. %v", err)
		return nil
	}
	ctx := context.Background()
	tok, err := yttokenFromFile(cacheFile)
	if err != nil {
		tok = ytgetTokenFromWeb(ctx, oauth2config)
		if tok == nil {
			log.Printf("unable to get auth token\n")
			return nil
		}
		ytsaveToken(cacheFile, tok)
	}
	log.Println("[yttoken] Expirey: ", tok.Expiry, ", isZero: ", tok.Expiry.IsZero(), ", isValid: ", tok.Valid())
	tokSource := oauth2config.TokenSource(ctx, tok)
	ntok, err := tokSource.Token()
	if err != nil {
		log.Printf("unable to get access token %v", err)
		return nil
	}
	if ntok.AccessToken != tok.AccessToken {
		ytsaveToken(cacheFile, ntok)
	}
	oauth2client := oauth2config.Client(ctx, ntok)
	ytclient, err := youtube.New(oauth2client)
	if err != nil {
		log.Printf("failed to create client: %v", err)
		return nil
	}
	return ytclient
}
