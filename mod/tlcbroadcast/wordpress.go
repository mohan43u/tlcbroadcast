package tlcbroadcast

import (
	"io"
	"log"
	"fmt"
	"bytes"
	"net/url"
	"strconv"
	"strings"
	"net/http"
	"encoding/json"
)

type WpHttpClient interface {
	Get(url string) (resp *http.Response, err error)
	Post(url string, contentType string, body io.Reader) (resp *http.Response, err error)
}

type WpUrls struct {
	MeUrlPathTemplate string
	PostsUrlPathTemplate string
	NewPostUrlPathTemplate string
	TagUrlPathTemplate string
	NewTagUrlPathTemplate string
	CategoryUrlPathTemplate string
	NewCategoryUrlPathTemplate string
}

type WpTag struct {
	Id int64 `json:"id"`
	Name string `json:"name"`
	Slug string `json:"slug"`
}

type WpCategory struct {
	Id int64 `json:"id"`
	Name string `json:"name"`
	Slug string `json:"slug"`
}

type WpClient struct {
	Client WpHttpClient
	Urls WpUrls
	Me *WpMe
}

type WpMe struct {
	Id int64 `json:"ID"`
	UserName string `json:"username"`
	PrimaryBlog int64 `json:"primary_blog"`
}

type WpPostsRespPost struct {
	Id int64 `json:"ID"`
	Tags interface{} `json:"tags"`
	Categories interface{} `json:"categories"`
}

type WpPostsRespMeta struct {
	NextPage string `json:"next_page"`
}

type WpPostsResp struct {
	Posts []WpPostsRespPost `json:"posts"`
}

func (self *WpPostsRespPost) GetTags() []string {
	tagsv := make([]string, 0)
	if tagis, ok := self.Tags.([]interface{}); ok == true {
		for _, tagi := range tagis {
			if tag, ok := tagi.(string); ok == true {
				tagsv = append(tagsv, tag)
			}

			if tag, ok := tagi.(float64); ok == true {
				tagsv = append(tagsv, strconv.FormatInt(int64(tag), 10))
			}
		}
	} else if tags, ok := self.Tags.(map[string]interface{}); ok == true {
		for tag := range tags {
			tagsv = append(tagsv, tag)
		}
	}
	return tagsv
}

func (self *WpPostsRespPost) CheckTag(itag string) bool {
	if tagis, ok := self.Tags.([]interface{}); ok == true {
		for _, tagi := range tagis {
			if tag, ok := tagi.(string); ok == true {
				if tag == itag {
					return true
				}
			}

			if tag, ok := tagi.(float64); ok == true {
				if strconv.FormatInt(int64(tag), 10) == itag {
					return true
				}
			}
		}
	} else if tags, ok := self.Tags.(map[string]interface{}); ok == true {
		for tag := range tags {
			if tag == itag {
				return true
			}
		}
	}
	return false
}

func (self *WpPostsRespPost) GetCategories() []string {
	categories := make([]string, 0)
	if Categories, ok := self.Categories.([]string); ok == true {
		for _, category := range Categories {
			categories = append(categories, category)
		}
	} else if Categories, ok := self.Categories.(map[string]interface{}); ok == true {
		for category := range Categories {
			categories = append(categories, category)
		}
	}
	return categories
}

func (self *WpPostsRespPost) CheckCategory(icategory string) bool {
	if Categories, ok := self.Categories.([]string); ok == true {
		for _, category := range Categories {
			if category == icategory {
				return true
			}
		}
	} else if Categories, ok := self.Categories.(map[string]interface{}); ok == true {
		for category := range Categories {
			if category == icategory {
				return true
			}
		}
	}
	return false
}

func WpGetClient() *WpClient {
	wpclient := WpGetClientOauth()
	if wpclient != nil {
		return wpclient
	}

	wpclient = WpGetClientBasicAuth()
	if wpclient != nil {
		return wpclient
	}

	log.Printf("No more way to create wordpress client")
	return nil
}

func (self *WpClient) GetTag(website string, tagname string) *WpTag {
	encoded_tagname := url.QueryEscape(tagname)
	if strings.Contains(self.Urls.TagUrlPathTemplate, "slug:") {
		tagname = strings.ReplaceAll(tagname, " ", "-")
	}

	tagurl := fmt.Sprintf(self.Urls.TagUrlPathTemplate, website, encoded_tagname)
	index := strings.LastIndex(tagurl, "%!")
	if index != -1 {
		tagurl = tagurl[:index]
	}
	resp, err := self.Client.Get(tagurl)
	if resp != nil && resp.StatusCode != 200 {
		log.Println("failed to get tag response")
		PrintErrorResponse(resp, err)
		return nil
	}

	respbytes, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Println("failed to read tag body")
		PrintErrorResponse(resp, err)
		return nil
	}

	var body interface{}
	if err := json.Unmarshal(respbytes, &body); err != nil {
		log.Println("failed to parse tag body")
		PrintErrorResponse(resp, err)
		return nil
	}

	if bodyv, ok := body.([]interface{}); ok == true {
		if len(bodyv) <= 0 {
			log.Println("empty body while parsing tag")
			return nil
		}

		for _, tagbody := range bodyv {
			tagbytes, err := json.Marshal(tagbody)
			if err != nil {
				log.Println("failed to marshal tag body")
				continue
			}

			tag := &WpTag{}
			if err := json.Unmarshal(tagbytes, tag); err != nil {
				log.Println("failed to unmarshal tag body")
				continue
			}

			if tag.Name == tagname {
				return tag
			}
		}
	} else {
		bodybytes, err := json.Marshal(body)
		if err != nil {
			log.Println("failed to marshal tag body")
			return nil
		}

		tag := &WpTag{}
		if err := json.Unmarshal(bodybytes, tag); err != nil {
			log.Println("failed to unmarshal tag body", err)
			return nil
		}
		return tag
	}
	return nil
}

func (self *WpClient) NewTag(website string, tagname string) *WpTag {
	tagurl := fmt.Sprintf(self.Urls.NewTagUrlPathTemplate, website)
	index := strings.LastIndex(tagurl, "%!")
	if index != -1 {
		tagurl = tagurl[:index]
	}

	body, err := json.Marshal(map[string]string{
		"name": tagname,
	})
	if err != nil {
		log.Print("failed to create tag body: ", err)
		return nil
	}

	resp, err := self.Client.Post(tagurl, "application/json", bytes.NewReader(body))
	if resp != nil &&
		(resp.StatusCode != 200 &&
			resp.StatusCode != 201) {
		log.Println("failed to get new tag response")
		PrintErrorResponse(resp, err)
		return nil
	}

	decoder := json.NewDecoder(resp.Body)
	tag := &WpTag{}
	if err := decoder.Decode(tag); err != nil {
		log.Println("failed to parse tag")
		PrintErrorResponse(resp, err)
		return nil
	}

	return tag
}

func (self *WpClient) GetCategory(website string, categoryname string) *WpCategory {
	encoded_categoryname := url.QueryEscape(categoryname)
	if strings.Contains(self.Urls.CategoryUrlPathTemplate, "slug:") {
		encoded_categoryname = strings.ReplaceAll(categoryname, " ", "-")
	}

	categoryurl := fmt.Sprintf(self.Urls.CategoryUrlPathTemplate, website, encoded_categoryname)
	index := strings.LastIndex(categoryurl, "%!")
	if index != -1 {
		categoryurl = categoryurl[:index]
	}
	resp, err := self.Client.Get(categoryurl)
	if resp != nil && resp.StatusCode != 200 {
		log.Println("failed to get category response")
		PrintErrorResponse(resp, err)
		return nil
	}

	respbytes, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Println("failed to read category body")
		PrintErrorResponse(resp, err)
		return nil
	}

	var body interface{}
	if err := json.Unmarshal(respbytes, &body); err != nil {
		log.Println("failed to parse category body")
		PrintErrorResponse(resp, err)
		return nil
	}

	if bodyv, ok := body.([]interface{}); ok == true {
		if len(bodyv) <= 0 {
			log.Println("empty body while parsing category")
			return nil
		}

		for _, categorybody := range bodyv {
			categorybytes, err := json.Marshal(categorybody)
			if err != nil {
				log.Println("failed to marshal category body")
				continue
			}

			category := &WpCategory{}
			if err := json.Unmarshal(categorybytes, category); err != nil {
				log.Println("failed to unmarshal category body")
				continue
			}

			if category.Name == categoryname {
				return category
			}
		}
	} else {
		bodybytes, err := json.Marshal(body)
		if err != nil {
			log.Println("failed to marshal category body")
			return nil
		}

		category := &WpCategory{}
		if err := json.Unmarshal(bodybytes, category); err != nil {
			log.Println("failed to unmarshal category body", err)
			return nil
		}

		return category
	}
	return nil
}

func (self *WpClient) NewCategory(website string, categoryname string) *WpCategory {
	categoryurl := fmt.Sprintf(self.Urls.NewCategoryUrlPathTemplate, website)
	index := strings.LastIndex(categoryurl, "%!")
	if index != -1 {
		categoryurl = categoryurl[:index]
	}

	body, err := json.Marshal(map[string]string{
		"name": categoryname,
	})
	if err != nil {
		log.Print("failed to create category body: ", err)
		return nil
	}

	resp, err := self.Client.Post(categoryurl, "application/json", bytes.NewReader(body))
	if resp != nil &&
		(resp.StatusCode != 200 &&
			resp.StatusCode != 201) {
		log.Println("failed to get new category response")
		PrintErrorResponse(resp, err)
		return nil
	}

	decoder := json.NewDecoder(resp.Body)
	category := &WpCategory{}
	if err := decoder.Decode(category); err != nil {
		log.Println("failed to parse category")
		PrintErrorResponse(resp, err)
		return nil
	}

	return category
}

func (self *WpClient) GetMe(website string) *WpMe {
	if self.Me == nil {
		meurl := fmt.Sprintf(self.Urls.MeUrlPathTemplate, website)
		index := strings.LastIndex(meurl, "%!")
		if index != -1 {
			meurl = meurl[:index]
		}
		resp, err := self.Client.Get(meurl)
		if resp != nil && resp.StatusCode != 200 {
			log.Println("failed to get me")
			PrintErrorResponse(resp, err)
			return nil
		}

		decoder := json.NewDecoder(resp.Body)
		wpme := &WpMe{}
		if err := decoder.Decode(wpme); err != nil {
			log.Println("failed to gen WpMe:", err)
			return nil
		}
		self.Me = wpme
	}

	return self.Me
}

func (self *WpClient) GetPosts(website string, iparams map[string]string) *[]WpPostsRespPost {
	posts := make([]WpPostsRespPost, 0)
	pagenum := 1
	for {
		if len(website) <= 0 &&
			self.Me != nil {
			website = strconv.FormatInt(self.Me.PrimaryBlog, 10)
		}

		var params string
		for key, value := range iparams {
			params = params + "&" + key + "=" + value
		}

		postsurl := fmt.Sprintf(self.Urls.PostsUrlPathTemplate + "?page=%d" + params, website, pagenum)
		index := strings.LastIndex(postsurl, "%!")
		if index != -1 {
			postsurl = postsurl[:index]
		}
		pagenum += 1

		resp, err := self.Client.Get(postsurl)
		if resp != nil &&
			(resp.StatusCode != 200) {
			if (resp.StatusCode != 400) {
				/* even though we got 400 which is what we want but
				   still put it in the logs, because we dont know
				   what the server sends
				*/
				PrintErrorResponse(resp, err)
			} else {
				log.Println("failed to get posts")
				PrintErrorResponse(resp, err)
				return nil
			}
		}

		respbytes, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Println("failed to read posts")
			PrintErrorResponse(resp, err)
			return nil
		}

		var body interface{}
		if err := json.Unmarshal(respbytes, &body); err != nil {
			log.Println("failed to parse posts")
			PrintErrorResponse(resp, err)
			return nil
		}

		if bodyv, ok := body.([]interface{}); ok == true {
			if len(bodyv) <= 0 {
				break
			}
			for _, postbody := range bodyv {
				postbytes, err := json.Marshal(postbody)
				if err != nil {
					log.Println("failed to marshal post")
					continue
				}

				post := &WpPostsRespPost{}
				if err := json.Unmarshal(postbytes, post); err != nil {
					log.Println("failed to unmarshal post")
					continue
				}
				posts = append(posts, *post)
			}
		} else {
			bodybytes, err := json.Marshal(body)
			if err != nil {
				log.Println("failed to marshal body")
				break
			}

			wppostsresp := &WpPostsResp{}
			if err := json.Unmarshal(bodybytes, wppostsresp); err != nil {
				log.Println("failed to unmarshal body", err)
				break
			}

			if len(wppostsresp.Posts) <= 0 {
				break
			}
			posts = append(posts, wppostsresp.Posts...)
		}
	}
	return &posts
}

func (self *WpClient) Post(website string, title string, content string, author string, tags string, categories string) bool {
	if len(website) <= 0 &&
		self.Me != nil &&
		self.Me.PrimaryBlog > 0 {
		website = strconv.FormatInt(self.Me.PrimaryBlog, 10)
	}

	if len(author) <= 0 &&
		self.Me != nil {
		if len(self.Me.UserName) > 0 {
			author = self.Me.UserName
		} else {
			author = strconv.FormatInt(self.Me.Id, 10)
		}
	}

	bodybyte, err := json.Marshal(map[string]string{
		"title": title,
		"content": content,
		"author": author,
		"tags": tags,
		"categories": categories,
		"status": "publish",
	})
	if err != nil {
		log.Println("failed to create post:", err)
		return false
	}

	newposturl := fmt.Sprintf(self.Urls.NewPostUrlPathTemplate, website)
	index := strings.LastIndex(newposturl, "%!")
	if index != -1 {
		newposturl = newposturl[:index]
	}

	resp, err := self.Client.Post(newposturl, "application/json", bytes.NewReader(bodybyte))
	if resp != nil &&
		(resp.StatusCode != 200 &&
			resp.StatusCode != 201) {
		log.Println("failed to post")
		PrintErrorResponse(resp, err)
		return false
	}
	return true
}

func (self *WpClient) Upload(website string, videos *[]YtVideo) bool {
	self.GetMe(website)
	for _, video := range *videos {
		if video.Status != "public" {
			log.Println("not published ", video.Url)
			continue
		}

		vidtag := self.GetTag(website, video.Id)
		if vidtag != nil {
			var vidtag_post *WpPostsRespPost
			fieldname := "tag"
			fieldvalue := vidtag.Name
			if self.Me == nil ||
				(self.Me != nil &&
					len(self.Me.UserName) <= 0) {
				fieldname = "tags"
				fieldvalue = strconv.FormatInt(vidtag.Id, 10)
			}
			posts := self.GetPosts(website, map[string]string{fieldname: fieldvalue})
			if posts == nil {
				log.Println("failure during getting posts for tag ", vidtag.Id)
				continue
			}
			for _, post := range *posts {
				tags := post.GetTags()
				for _, tag := range tags {
					if !(tag != strconv.FormatInt(vidtag.Id, 10) && tag != vidtag.Name) {
						vidtag_post = &post
						break
					}
				}
				if vidtag_post != nil {
					break
				}
			}
			if vidtag_post != nil {
				log.Println("wordpress: already uploaded ", video.Url)
				continue
			}
		}

		if vidtag == nil {
			vidtag = self.NewTag(website, video.Id)
			if vidtag == nil {
				log.Println("cannot create vid tag")
				return false
			}
		}

		tlctag := self.GetTag(website, "tlc")
		if tlctag == nil {
			log.Println("not able to get tlc tag")
			return false
		}

		tlccategory := self.GetCategory(website, "tamil linux community")
		if tlccategory == nil {
			log.Println("cannot get tamil linux community category details")
			return false
		}

		videoscategory := self.GetCategory(website, "videos")
		if videoscategory == nil {
			log.Println("cannot get videos category details")
			return false
		}

		tlctagname := tlctag.Name
		vidtagname := vidtag.Name
		tlccategoryname := tlccategory.Name
		videoscategoryname := videoscategory.Name
		if self.Me == nil ||
			(self.Me != nil &&
				len(self.Me.UserName) <= 0) {
			tlctagname = strconv.FormatInt(tlctag.Id, 10)
			vidtagname = strconv.FormatInt(vidtag.Id, 10)
			tlccategoryname = strconv.FormatInt(tlccategory.Id, 10)
			videoscategoryname = strconv.FormatInt(videoscategory.Id, 10)
		}
		if self.Post(website,
			video.Title,
			video.Url + "\n" + video.Description,
			"",
			tlctagname + ", " + vidtagname,
			tlccategoryname + ", " + videoscategoryname) {
			log.Println("wordpress: uploaded ", video.Url)
		}
	}
	return true
}
