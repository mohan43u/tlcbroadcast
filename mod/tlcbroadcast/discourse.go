package tlcbroadcast

import (
	"os"
	"log"
	"time"
	"strings"
	"os/user"
	"net/http"
	"path/filepath"
	"encoding/json"
)

type DcCategory struct {
	Id int64 `json:"id"`
	Name string `json:"name"`
	Slug string `json:"slug"`
}

type DcCategoryList struct {
	Categories []DcCategory `json:"categories"`
}

type DcCategoryResp struct {
	CategoryList DcCategoryList `json:"category_list"`
}

type DcTopic struct {
	Title string `json:"title"`
	Raw string `json:"raw"`
	CategoryId int64 `json:"category"`
	ExternalId string `json:"external_id"`
}

type DcClient struct {
	ApiKey string `json:"api_key"`
	ApiUsername string `json:"api_username"`
	WebSite string
	Categories *[]DcCategory
	Client *http.Client
}

func (self *DcClient) Get(uri string) (*http.Response, error) {
	request, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		log.Printf("failed to create request: %v\n", err)
		return nil, err
	}
	request.Header.Set("Api-Key", self.ApiKey)
	request.Header.Set("Api-Username", self.ApiUsername)
	return self.Client.Do(request)
}

func (self *DcClient) Post(uri string, body string) (*http.Response, error) {
	reader := strings.NewReader(body)
	request, err := http.NewRequest("POST", uri, reader)
	if err != nil {
		log.Printf("failed to create request: %v\n", err)
		return nil, err
	}
	request.Header.Set("Api-Key", self.ApiKey)
	request.Header.Set("Api-Username", self.ApiUsername)
	request.Header.Set("Content-Type", "application/json")
	return self.Client.Do(request)
}

func (self *DcClient) CheckId(id string) bool {
	for {
		uri := self.WebSite + "/t/external_id/" + id + ".json"
		resp, err := self.Get(uri)
		if err != nil {
			log.Printf("failed to process external_id: %v\n", id)
			PrintErrorResponse(resp, err)
			return false
		}
		if resp.StatusCode == 429 {
			log.Println("Too Many Requests closing Idle Connections")
			time.Sleep(10 * time.Second)
			self.Client.CloseIdleConnections()
			continue
		}
		if resp.StatusCode != 200 {
			log.Printf("failed to get external_id: %v\n", id)
			PrintErrorResponse(resp, err)
			return false
		}
		break
	}
	return true
}

func (self *DcClient) GetCategories() *[]DcCategory {
	if self.Categories == nil {
		for {
			uri := self.WebSite + "/categories.json"
			resp, err := self.Get(uri)
			if err != nil {
				log.Println("failed to process getcategory request")
				PrintErrorResponse(resp, err)
				return nil
			}
			if resp.StatusCode == 429 {
				log.Println("Too Many Requests closing Idle Connections")
				time.Sleep(10 * time.Second)
				self.Client.CloseIdleConnections()
				continue
			}
			if resp.StatusCode != 200 {
				log.Println("failed to get categories")
				PrintErrorResponse(resp, err)
				return nil
			}
			cresp := &DcCategoryResp{}
			decoder := json.NewDecoder(resp.Body)
			if err := decoder.Decode(cresp); err != nil {
				log.Println("failed to parse categories")
				PrintErrorResponse(resp, err)
				return nil
			}

			categories := append(make([]DcCategory, 0), cresp.CategoryList.Categories...)
			self.Categories = &categories
			break
		}
	}
	return self.Categories
}

func (self *DcClient) GetCategoryId(slug string) int64 {
	categories := self.GetCategories()
	if categories == nil {
		log.Println("failed to get categories")
		return -1
	}
	for _, category := range *categories {
		if category.Slug == slug {
			return category.Id
		}
	}
	return -1
}

func (self *DcClient) Upload(videos *[]YtVideo) bool {
	for _, video := range *videos {
		if video.Status != "public" {
			log.Println("not published ", video.Url)
			continue
		}
		for {
			uri := self.WebSite + "/posts.json"
			post := &DcTopic{
				Title: "[யூடியூப் நிகழ்படம்] " + video.Title,
				Raw: video.Url + "\n" + video.Description,
				CategoryId: self.GetCategoryId("news"),
				ExternalId: video.Id,
			}
			body := &strings.Builder{}
			encoder := json.NewEncoder(body)
			if err := encoder.Encode(post); err != nil {
				log.Printf("failed to encode body: %v\n", err)
				return false
			}
			resp, err := self.Post(uri, body.String())
			if err != nil {
				log.Printf("failed processing create Topic: %v\n", video.Url)
				PrintErrorResponse(resp, err)
				return false
			}
			if resp.StatusCode == 429 {
				log.Println("Too Many Requests closing Idle Connections")
				time.Sleep(10 * time.Second)
				self.Client.CloseIdleConnections()
				continue
			}
			if resp.StatusCode == 422 {
				log.Println("discourse: already uploaded ", video.Url)
				break
			}
			if resp.StatusCode != 200 {
				log.Printf("failed to create Topic: %v\n", video.Url)
				PrintErrorResponse(resp, err)
				break
			}
			log.Println("discourse: uploaded ", video.Url)
			break
		}
	}
	return true
}

func dcConfigFile() (string, error) {
	usr, err := user.Current()
	if err != nil {
		return "", err
	}
	configDir := os.Getenv("XDG_CONFIG_HOME")
	if len(configDir) <= 0 {
		configDir = filepath.Join(usr.HomeDir, ".config")
	}
	configDir = filepath.Join(configDir,  "tlcbroadcast")
	os.MkdirAll(configDir, 0700)
	return filepath.Join(configDir,"dcclient.json"), err
}

func DcGetClient(website string) *DcClient {
	file, err := dcConfigFile()
	if err != nil {
		log.Printf("failed to get config file: %v\n", err)
		return nil
	}
	f, err := os.Open(file)
	if err != nil {
		log.Printf("failed to open config file: %v\n", err)
		return nil
	}
	defer f.Close()
	client := &DcClient{}
	err = json.NewDecoder(f).Decode(client)
	if err != nil {
		log.Printf("failed to decode config file: %v\n", err)
		return nil
	}
	if (strings.HasPrefix(website, "http://") || strings.HasPrefix(website, "https://")) == false {
		website = "https://" + website
	}
	client.WebSite = website
	client.Client = &http.Client{}
	return client
}
