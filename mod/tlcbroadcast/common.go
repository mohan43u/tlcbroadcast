package tlcbroadcast

import (
	"bufio"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"
)

type AuthTokenHandler struct {
	Cmd *exec.Cmd
	Server *http.Server
	Code string
	Stop chan bool
}

func (self *AuthTokenHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	query := req.URL.Query()
	self.Code = query.Get("code")
	if len(self.Code) <= 0 {
		log.Printf("unable to get code from %v\n", req.URL)
	}
	if self.Stop != nil {
		self.Stop <- true
	}
	if self.Cmd != nil {
		self.Cmd.Process.Kill()
	}
	if self.Server != nil {
		self.Server.Close()
	}
}

func FindExe(desktopfile string) string {
	home := os.Getenv("HOME")
	datadirs := os.Getenv("XDG_DATA_DIRS")
	datahome := os.Getenv("XDG_DATA_HOME")
	var exe string

	if len(datadirs) <= 0 {
		datadirs = "/usr/local/share/:/usr/share/"
	}
	if len(datahome) <= 0 {
		datahome = home + "/.local/share"
	}
	dirs := append(make([]string, 0), datahome)
	dirs = append(dirs, strings.Split(datadirs, ":")...)
	for _, dir := range dirs {
		filepath := dir + "/applications/" + desktopfile
		file, err := os.Open(filepath)
		if err != nil {
			continue
		}
		reader := bufio.NewReader(file)
		var line string
		for {
			buf, isprefix, err := reader.ReadLine()
			line += string(buf)
			if isprefix {
				continue
			}
			if strings.HasPrefix(line, "Exec=") {
				exe = strings.Split(line, "=")[1]
				if len(exe) > 0 {
					exe = strings.Split(exe, " ")[0]
				}
				break
			}
			line = ""
			if err != nil {
				break
			}
		}
	}
	return exe
}

func GetAuthCode(authurl string) string {
	cmd := exec.Command("xdg-mime", "query", "default", "text/html")
	output, err := cmd.Output()
	if err != nil {
		log.Printf("failed to get application for text/html\n")
		return ""
	}
	desktopfile := strings.TrimSpace(string(output))
	exe := FindExe(desktopfile)
	if len(exe) <= 0 {
		log.Printf("failed to get exec for %s\n", desktopfile)
		return ""
	}
	cmd = exec.Command(exe, authurl)
	if err = cmd.Start(); err != nil {
		log.Printf("failed to invoke browser for url %s (err: %v)\n", authurl, err)
		return ""
	}

	server := new(http.Server)
	handler := new(AuthTokenHandler)
	handler.Stop = make(chan bool)
	handler.Cmd = cmd
	handler.Server = server
	server.Addr = "127.0.0.1:8888"
	server.Handler = handler
	timer := time.NewTimer(5 * time.Minute)
	go func() {
		select {
		case timeout := <-timer.C:
			log.Printf("timeout: %v\n", timeout)
			if err := cmd.Process.Kill(); err != nil {
				log.Printf("failed to kill (%v) after timeout (%v): %v\n", cmd, timeout, err)
			}
			if err := server.Shutdown(nil); err != nil {
				log.Printf("failed to shutdown after timeout (%v): %v\n", timeout, err)
			}
		case <-handler.Stop:
			log.Println("handler asking to windup")
		}
	}()
	err = server.ListenAndServe()
	if len(handler.Code) <= 0 && err != nil {
		log.Printf("http server failed: %v\n", err)
		return ""
	}
	log.Printf("code: %s\n", handler.Code)
	return handler.Code
}

func PrintResponse(resp *http.Response) {
	log.Printf("req: %+v\n", resp.Request)
	log.Printf("resp: %+v\n", resp)
	bytes, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("failed to read body: %v\n", err)
		return
	}
	log.Print(string(bytes))
}

func PrintErrorResponse(resp *http.Response, err error) {
	log.Printf("err: %+v\n", err)
	PrintResponse(resp)
}
