### TlcBroadcast

Simple tool to upload TamilLinuxCommunity youtube channel videos to Wordpress

#### Installation

* Copy (or symlink) `mod/tlcbroadcast/ytclient.json` to `~/.config/tlcbroadcast` directory
* Copy (or symlink) `mod/tlcbroadcast/wpclient.json` to `~/.config/tlcbroadcast` directory
* Run `CGO_ENABLED=0 go build` inside `mod/tlcbroadcast/tlcbroadcast` directory
* run `./tlcbroadcast` inside `mod/tlcbroadcast/tlcbroadcast` directory
